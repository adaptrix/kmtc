<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.index');
});
Route::get('/about', function () {
    return view('pages.about');
});

Route::group(["prefix"=>"services"],function(){
    Route::get('contract',function(){
        return view('pages.services.contract',["service_name"=>"Electrical contracting services"]);
    });
    Route::get('k-track',function(){
        return view('pages.services.k-track',["service_name"=>"K Track"]);
    });
    Route::get('solar-heating',function(){
        return view('pages.services.solar-heating',["service_name"=>"Solar heating"]);
    });
    Route::get('laundry-equipment',function(){
        return view('pages.services.laundry-equipment',["service_name"=>"Laundry equipment"]);
    });
    Route::get('generator-services',function(){
        return view('pages.services.generator-services',["service_name"=>"Generator services"]);
    });
    Route::get('installation-and-commisioning',function(){
        return view('pages.services.installation-and-commisioning',["service_name"=>"Installation and commisioning"]);
    });
    Route::get('systems',function(){
        return view('pages.services.systems',["service_name"=>"Systems trouble shoot and repair"]);
    });
    Route::get('switch-gear',function(){
        return view('pages.services.switch-gear',["service_name"=>"Switch gear and switch board preventive maintenance"]);
    });
    Route::get('fire-prevention',function(){
        return view('pages.services.fire-prevention',["service_name"=>"Fire prevention services"]);
    });
});

Route::get('/contact', function () {
    return view('pages.contact');
}); 
