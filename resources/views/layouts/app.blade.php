<!DOCTYPE html>
<html lang="en">
@include('layouts.head')
<body>
    <div class="body-inner">
        {{-- <div class="preload"></div> --}}
        @include('layouts.header')
        @yield('contents')
        @include('layouts.footer')
        @include('layouts.scripts')
    </div><!-- Body inner end -->
</body>

</html>