
	<!-- Javascript Files
	================================================== -->

	<!-- initialize jQuery Library -->
	<script type="text/javascript" src="{{asset('js/jquery.js')}}"></script>
	<!-- Bootstrap jQuery -->
	<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
	<!-- Owl Carousel -->
	<script type="text/javascript" src="{{asset('js/owl.carousel.min.js')}}"></script>
	<!-- Counter -->
	<script type="text/javascript" src="{{asset('js/jquery.counterup.min.js')}}"></script>
	<!-- Waypoints -->
	<script type="text/javascript" src="{{asset('js/waypoints.min.js')}}"></script>
	<!-- Color box -->
	<script type="text/javascript" src="{{asset('js/jquery.colorbox.js')}}"></script>
	<!-- Smoothscroll -->
	<script type="text/javascript" src="{{asset('js/smoothscroll.js')}}"></script>
	<!-- Isotope -->
	<script type="text/javascript" src="{{asset('js/isotope.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/ini.isotope.js')}}"></script>

	<!-- Template custom -->
	<script type="text/javascript" src="{{asset('js/custom.js')}}"></script>