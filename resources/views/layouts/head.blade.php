<head>

	<!-- Basic Page Needs
	================================================== -->
	<meta charset="utf-8">
	<title>Kimtech Company Limited</title>

	<!-- Mobile Specific Metas
	================================================== -->

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

	<!-- SEO META TAGS -->
	<meta name="title" content="Kim tech company limited">
	<meta name="description" content="A Tanzania based technology company that is focused on delivering various services including K track, solar heating and fire prevention services">
	<meta name="keywords" content="Electrical, Tanzania, Company, Ltd, Technology, Fire, Prevention, Tracking, Solar, Heating, Laundry, Equipment, Installation, Commission, Contractor, Contracting, ">
	<meta name="robots" content="index, follow">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="English">
	<meta name="revisit-after" content="8 days">
	<meta name="author" content="Kim tech company limited">

	<!--Favicon-->
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">

	<!-- CSS
	================================================== -->
	
	<!-- Bootstrap -->
	<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
	<!-- Template styles-->
	<link rel="stylesheet" href="{{asset('css/style.css')}}">
	<!-- Responsive styles-->
	<link rel="stylesheet" href="{{asset('css/responsive.css')}}">
	<!-- FontAwesome -->
	<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
	<!-- Animation -->
	<link rel="stylesheet" href="{{asset('css/animate.css')}}">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">
	<!-- Colorbox -->
	<link rel="stylesheet" href="{{asset('css/colorbox.css')}}">
	<link rel="stylesheet" href="{{asset('css/custom.css')}}">

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
	<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<script src="js/respond.min.js"></script>
	 <![endif]-->

</head>