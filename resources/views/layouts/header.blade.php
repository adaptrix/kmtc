
	<div id="top-bar" class="top-bar">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
					<ul class="top-info">
						<li><i class="fa fa-phone">&nbsp;</i><p class="info-text">+255 753 513 125</p></li>
						<li><i class="fa fa-envelope-o">&nbsp;</i><p class="info-text">info@kimtech.co.tz</p></li>
					</ul>
				</div><!--/ Top info end -->

				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 top-social text-right">
					<ul class="unstyled">
						<li>
							<a title="Facebook" href="#">
								<span class="social-icon"><i class="fa fa-facebook"></i></span>
							</a>
							<a title="Twitter" href="#">
								<span class="social-icon"><i class="fa fa-twitter"></i></span>
							</a>
							<a title="Google+" href="#">
								<span class="social-icon"><i class="fa fa-google-plus"></i></span>
							</a>
							<a title="Linkdin" href="#">
								<span class="social-icon"><i class="fa fa-linkedin"></i></span>
							</a>
							<a title="Rss" href="#">
								<span class="social-icon"><i class="fa fa-rss"></i></span>
							</a>
							<a title="Skype" href="#">
								<span class="social-icon"><i class="fa fa-skype"></i></span>
							</a>
						</li>
					</ul>
				</div><!--/ Top social end -->
			</div><!--/ Content row end -->
		</div><!--/ Container end -->
	</div><!--/ Topbar end -->

	<!-- Header start -->
	<header id="header" class="nav-style-boxed">
		<div class="container">
			<div class="row">
				<div class="logo-area clearfix">
					<div class="logo col-xs-12 col-md-3">
						 <a href="{{url('/')}}">
							<img src="{{asset('images/logo.png')}}" alt="">
						 </a>
					</div><!-- logo end -->

					<div class="col-xs-12 col-md-9 header-right">
						<ul class="top-info-box">
							<li>
								<div class="info-box"><span class="info-icon"><i class="fa fa-map-marker">&nbsp;</i></span>
									<div class="info-box-content">
										<p class="info-box-title">Find Us</p>
										<p class="info-box-subtitle">Arusha, Tanzania</p>
									</div>
								</div>
							</li>
							<li>
								<div class="info-box"><span class="info-icon"><i class="fa fa-phone">&nbsp;</i></span>
									<div class="info-box-content">
										<p class="info-box-title">Call Us</p>
										<p class="info-box-subtitle">+255 753 513 125</p>
									</div>
								</div>
							</li>
							<li>
								<div class="info-box last"><span class="info-icon"><i class="fa fa-compass">&nbsp;</i></span>
									<div class="info-box-content">
										<p class="info-box-title">We are Open</p>
										<p class="info-box-subtitle">10.00 AM - 6.00 Pm</p>
									</div>
								</div>
							</li>
							 <li class="nav-search">
								<span id="search"><i class="fa fa-search"></i></span>
							</li>
						</ul>
						<div class="search" style="display: none;">
							<input type="text" class="form-control" placeholder="Type what you want and enter">
							<span class="search-close">&times;</span>
						</div><!-- Site search end -->
					</div><!-- header right end -->
				</div><!-- logo area end -->
				
			</div><!-- Row end -->
		</div><!-- Container end -->

		<nav class="site-navigation navigation">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="site-nav-inner pull-left">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							  <span class="sr-only">Toggle navigation</span>
							  <span class="icon-bar"></span>
							  <span class="icon-bar"></span>
							  <span class="icon-bar"></span>
							</button>

							<div class="collapse navbar-collapse navbar-responsive-collapse">
								<ul class="nav navbar-nav">
									<li class="{{Request::is('/')?'active':''}}">
										<a href="{{url('/')}}">Home</a>
									</li>

									<li class="{{Request::is('about')?'active':''}}">
										<a href="{{url('about')}}">Who we are</a>
									</li> 

									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">Services <i class="fa fa-angle-down"></i></a>
										<ul class="dropdown-menu" role="menu">
												<li class="{{Request::is('services/contract')?'active':''}}">
													<a href="{{url('services/contract')}}">Electrical contracting services</a>
												</li> 
												<li class="{{Request::is('services/k-track')?'active':''}}">
													<a href="{{url('services/k-track')}}">K track</a>
												</li> 
												<li class="{{Request::is('services/solar-heating')?'active':''}}">
													<a href="{{url('services/solar-heating')}}">Solar heating</a>
												</li> 
												<li class="{{Request::is('services/laundry-equipment')?'active':''}}">
													<a href="{{url('services/laundry-equipment')}}">Laundry Equipment</a>
												</li> 
												<li class="{{Request::is('services/fire-prevention')?'active':''}}">
													<a href="{{url('services/fire-prevention')}}">Fire prevention services</a>
												</li> 
												<li class="{{Request::is('services/generator-services')?'active':''}}">
													<a href="{{url('services/generator-services')}}">Generator services</a>
												</li> 
												<li class="{{Request::is('services/installation-and-commisioning')?'active':''}}">
													<a href="{{url('services/installation-and-commisioning')}}">Installation and commisioning</a>
												</li> 
												<li class="{{Request::is('services/systems')?'active':''}}">
													<a href="{{url('services/systems')}}">Systems trouble shoot and repair</a>
												</li> 
												<li class="{{Request::is('services/switch-gear')?'active':''}}">
													<a href="{{url('services/switch-gear')}}">Switch gear and switch board preventive maintenance</a>
												</li>  
										</ul>
									</li>  

									<li class="{{Request::is('contact')?'active':''}}">
										<a href="{{url('contact')}}">Contact</a>
									</li>

									<li class="header-get-a-quote">
										<a href="{{url('contact')}}">Get Free Quote</a>
									</li>

								</ul><!--/ Nav ul end -->
							</div><!--/ Collapse end -->

						</div><!-- Site Navbar inner end -->

					</div><!--/ Col end -->
				</div><!--/ Row end -->
			</div><!--/ Container end -->

		</nav><!--/ Navigation end -->
	</header><!--/ Header end -->
