<footer id="footer" class="footer bg-overlay">
    <div class="footer-main">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 footer-widget footer-about">
                    <h3 class="widget-title">About Us</h3>
                    <img class="footer-logo" src="images/logo.png" alt="" />
                    <p><strong>"We are committed to providing the highest quality products, competitively priced, with services
                        exceeding our customer’s expectations. We will continue to invest in facilities, systems and
                        highly trained technical personnel providing added-value to our business relationships."</strong>- Our vision</p>
                    <div class="footer-social">
                        <ul>
                            <li><a href="#"><i class="fa fa-rss"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div><!-- Footer social end -->
                </div><!-- Col end -->

                <div class="col-md-4 col-sm-12 footer-widget">
                    <h3 class="widget-title">Services</h3>
                    <ul class="list-arrow">
                        <li><a href="{{url('services/k-track')}}">K track</a></li>
                        <li><a href="{{url('services/solar-heating')}}">Solar heating</a></li>
                        <li><a href="{{url('services/fire-prevention')}}">Fire prevention</a></li>
                        <li><a href="{{url('services/generator-services')}}">Generator services</a></li>
                        <li><a href="{{url('services/contract')}}">Electrical contracting</a></li>
                        <li><a href="{{url('services/installation-and-commisioning')}}">Installation and commision</a></li>
                    </ul>
                </div><!-- Col end -->

                <div class="col-md-4 col-sm-12 footer-widget">
                    <h3 class="widget-title">Working Hours</h3>
                    <div class="working-hours">
                        We work 7 days a week, every day excluding major holidays. Contact us if you have an emergency,
                        with our Hotline and Contact form.
                        <br><br> Monday - Friday: <span class="text-right">7:00 - 17:00 </span>
                        <br> Saturday: <span class="text-right">9:00 - 15:00</span>
                        <br> Sunday and holidays: <span class="text-right">09:00 - 12:00</span>
                    </div>
                </div><!-- Col end -->

            </div><!-- Row end -->
        </div><!-- Container end -->
    </div><!-- Footer main end -->

    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 text-center">
                    <div class="copyright-info">
                        <span>Copyright © {{\Carbon\Carbon::now()->year}} Kim tech company ltd. All Rights
                            Reserved.</span>
                    </div>
                </div> 
            </div><!-- Row end -->

            <div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix">
                <button class="btn btn-primary" title="Back to Top">
                    <i class="fa fa-angle-double-up"></i>
                </button>
            </div>

        </div><!-- Container end -->
    </div><!-- Copyright end -->

</footer><!-- Footer end -->