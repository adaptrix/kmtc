@extends('layouts.app')

@section('contents')	

	<!-- Carousel -->
	<div id="main-slide" class="carousel slide" data-ride="carousel">

		<!-- Indicators -->
		<ol class="carousel-indicators visible-lg visible-md">
			<li data-target="#main-slide" data-slide-to="0" class="active"></li>
			<li data-target="#main-slide" data-slide-to="1"></li>
			<li data-target="#main-slide" data-slide-to="2"></li>
		</ol><!--/ Indicators end-->

		<!-- Carousel inner -->
		<div class="carousel-inner">

			<div class="item active" style="background-image:url(images/slider-main/bg1.jpg)">
				<div class="slider-content text-left">
					<div class="col-md-12">
						<h2 class="slide-title-box animated2">5 Years Young</h2>
						<h3 class="slide-title animated3">K Track</h3>
						<h3 class="slide-sub-title animated3">Total Tracking Solutions</h3>  
						<p class="slider-description lead animated3">K Track comes with many features that can help companies from different industries. </p> 
						<p class="animated3">
							<a href="{{url('contract')}}" class="slider btn btn-primary">Get a Quuote</a>
							<a href="{{url('services/k-track')}}" class="slider btn btn-primary border">Lean more</a>
						</p>       
					</div>
				</div>
			</div><!--/ Carousel item 1 end -->

			<div class="item" style="background-image:url(images/services/solar-heating.jpg)">
				<div class="slider-content">
					<div class="col-md-12 text-center">
						<h2 class="slide-title animated4">Affordable heating solution</h2>
						<h3 class="slide-sub-title animated5">Solar Heating Services</h3>   
						<p class="slider-description lead animated3">We insure sufficient heating through the day and through the night, uninterrupted</p>
						 <p>
							<a href="{{url('contract')}}" class="slider btn btn-primary">Get a Quote</a>
							<a href="{{url('services/solar-heating')}}" class="slider btn btn-primary border">Learn more</a>
						 </p>      
					</div>
				</div>
			</div><!--/ Carousel item 2 end -->

			<div class="item" style="background-image:url(images/services/fp.jpg)">
				<div class="slider-content text-right">
					<div class="col-md-12">
						 <h2 class="slide-title animated6">Better safe than sorry</h2>
						 <h3 class="slide-sub-title animated7">Fire prevention services</h3> 
						 <p class="slider-description lead animated7">We are dedicated to providing our clients with professional safety equipment and fire preventive measures.</p>
						 <p>
							<a href="{{url('contract')}}" class="slider btn btn-primary">Get Free Quote</a>
							<a href="{{url('services/fire-prevention')}}" class="slider btn btn-primary border">Learn More</a>
						 </p>      
					</div>
				</div>
			 </div><!--/ Carousel item 3 end -->
			 
		</div><!-- Carousel inner end-->

		<!-- Controllers -->
		<a class="left carousel-control" href="#main-slide" data-slide="prev">
			<span><i class="fa fa-angle-left"></i></span>
		</a>
		<a class="right carousel-control" href="#main-slide" data-slide="next">
			<span><i class="fa fa-angle-right"></i></span>
		</a>
	</div><!--/ Carousel end -->  

	<section id="ts-features-top" class="ts-features-top">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="ts-feature-box">
						<div class="ts-feature-image-box image-angle one">
							<img class="img-responsive" src="images/services-front/laundry.png" alt="">
						</div>
						<div class="ts-feature-content">
							<h3 class="feature-box-title">Laundry equipment</h3>
							<p>We install fix and maintain laundry equipment for various facilities, including hotels..</p>
							<p><a class="learn-more" href="{{url('services/laundry-equipment')}}"><i class="fa fa-caret-right"></i> Learn More</a></p>  
						</div>
					</div><!-- Feature1 end -->
				</div><!-- Col 1 end -->

				<div class="col-md-4">
					<div class="ts-feature-box">
						<div class="ts-feature-image-box image-angle two">
							<img class="img-responsive" src="images/services-front/generator1.jpg" alt="">
						</div>
						<div class="ts-feature-content">
							<h3 class="feature-box-title">Generator services</h3>
							<p>We offer service and maintenance plans for commercial-grade industrial generators and equipment. </p>
							<p><a class="learn-more" href="{{url('services/generator-services')}}"><i class="fa fa-caret-right"></i> Learn More</a></p>  
						</div>
					</div><!-- Feature2 end -->
				</div><!-- Col 2 end --> 

				<div class="col-md-4">
					<div class="ts-feature-box">
						<div class="ts-feature-image-box image-angle three">
							<img class="img-responsive" src="images/services-front/electrical-contracting.jpg" alt="">
						</div>
						<div class="ts-feature-content">
							<h3 class="feature-box-title">Electrical contracting services</h3>
							<p>We provide a wide range of contracting services in the electrical field, in maintenance, installation.</p>
							<p><a class="learn-more" href="{{url('services/contract')}}"><i class="fa fa-caret-right"></i> Learn More</a></p>  
						</div>
					</div><!-- Feature3 end -->
				</div><!-- Col 3 end -->
			</div><!-- Content row end -->
			<div class="row">
				<div class="col-md-4">
					<div class="ts-feature-box">
						<div class="ts-feature-image-box image-angle one">
							<img class="img-responsive" src="images/services-front/switch-g.jpg" alt="">
						</div>
						<div class="ts-feature-content">
							<h3 class="feature-box-title">Switch gear and switch board preventive maintenance</h3>
							<p>Power Systems & Controls, Switchgear, Switchboard, and Circuit Breaker Preve..</p>
							<p><a class="learn-more" href="{{url('services/switch-gear')}}"><i class="fa fa-caret-right"></i> Learn More</a></p>  
						</div>
					</div><!-- Feature1 end -->
				</div><!-- Col 1 end -->

				<div class="col-md-4">
					<div class="ts-feature-box">
						<div class="ts-feature-image-box image-angle two">
							<img class="img-responsive" src="images/services-front/system.jpg" alt="">
						</div>
						<div class="ts-feature-content">
							<h3 class="feature-box-title">System trouble shoot and repair</h3>
							<p>Having a resource available to fix aging equipment is imperative for today’s high-tech, fast moving companies. </p>
							<p><a class="learn-more" href="{{url('services/systems')}}"><i class="fa fa-caret-right"></i> Learn More</a></p>  
						</div>
					</div><!-- Feature2 end -->
				</div><!-- Col 2 end -->

				<div class="col-md-4">
					<div class="ts-feature-box">
						<div class="ts-feature-image-box image-angle three">
							<img class="img-responsive" src="images/services-front/air-cond.jpg" alt="">
						</div>
						<div class="ts-feature-content">
							<h3 class="feature-box-title">Installation and commisioning</h3>
							<p>We are providing Air Conditioner installation and refrigeration systems. We provide best services like split air conditioner installation..</p>
							<p><a class="learn-more" href="{{url('services/installation-and-commisioning')}}"><i class="fa fa-caret-right"></i> Learn More</a></p>  
						</div>
					</div><!-- Feature3 end -->
				</div><!-- Col 3 end -->
			</div><!-- Content row end -->
		</div><!-- Container end -->
	</section><!-- Feature are end -->

	<section id="ts-quote-block" class="ts-quote-block pattern-bg">
		<div class="container">
			<div class="row">
				<div class="col-md-6">

					<h3 class="border-title border-left">Why Work With Us?</h3>

					<div class="panel-group" id="accordion">
						<div class="panel panel-default">
							 <div class="panel-heading">
								 <h4 class="panel-title"> 
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">INTEGRITY</a> 
								 </h4>
							 </div>
							 <div id="collapseOne" class="panel-collapse collapse in">
								<div class="panel-body"> 
									<p>•   We act with integrity and show respect. <br>
										•	Demonstrate a commitment to integrity and ethics. <br>
										•	Listen to others for understanding. <br>
										•	Assume positive intent. <br>
										</p>
								</div>
							 </div>
						</div><!--/ Panel 1 end-->

						<div class="panel panel-default">
							 <div class="panel-heading">
								 <h4 class="panel-title">
									<a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseTwo">ACCOUNTABILITY</a>
								</h4>
							 </div>
							 <div id="collapseTwo" class="panel-collapse collapse">
								<div class="panel-body"> 
									<p>•	We are all accountable. <br>
										•	Keep promises and commitments made to others. <br>
										•	Focus on finding solutions and achieving results. <br>
										•	Involve others in decisions and plans that affect </p>
								</div>
							 </div>
						</div><!--/ Panel 2 end-->

						<div class="panel panel-default">
							 <div class="panel-heading">
								 <h4 class="panel-title">
									<a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseThree"> PASSION</a>
								</h4>
							 </div>
							 <div id="collapseThree" class="panel-collapse collapse">
								<div class="panel-body"> 
									<p>•	We are passionate about our business. <br>
										•	Show pride in our company. <br>
										•	Promote a positive, optimistic and fun environment. <br>
										•	Value, promote and fiercely protect our reputation. <br>
										</p>
								</div>
							 </div>
						</div><!--/ Panel 3 end-->

						<div class="panel panel-default">
							 <div class="panel-heading">
								 <h4 class="panel-title">
									<a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseFour"> HUMILITY</a>
								</h4>
							 </div>
							 <div id="collapseFour" class="panel-collapse collapse">
								<div class="panel-body"> 
									<p>•   We have the humility and hunger to learn. <br>
										•	Seek and provide honest feedback. <br>
										•	Never underestimate our competition. 
										</p>
								</div>
							 </div>
						</div><!--/ Panel 3 end-->

						<div class="panel panel-default">
							 <div class="panel-heading">
								 <h4 class="panel-title">
									<a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseFive"> RESULTS</a>
								</h4>
							 </div>
							 <div id="collapseFive" class="panel-collapse collapse">
								<div class="panel-body"> 
									<p>•	We love success. <br>
										•	Make the tough calls. <br>
										•	Achieve results and celebrate when we do. <br>
										
										</p>
								</div>
							 </div>
						</div><!--/ Panel 3 end-->

					</div><!--/ Accordion end -->
					
				</div><!-- Block left -->

				<div class="col-md-6">
					<h3 class="border-title border-left">Get a Quick Quote</h3>

					<form id="contact-form" action="http://themewinter.com/html/cornike/logistic/contact-form.php" method="post" role="form">
						<div class="error-container"></div>
						<div class="row">
							<div class="col-xs-12 col-md-6">
								<div class="form-group">
								<input class="form-control form-control-name" name="name" id="name" placeholder="Name" type="text" required>
								</div>
							</div>

							<div class="col-xs-12 col-md-6">
								<div class="form-group">
									<input class="form-control form-control-email" name="email" id="email" 
									placeholder="Email" type="email" required>
								</div>
							</div>
						</div><!-- row end -->

						<div class="row">
							<div class="col-xs-12 col-md-6">
								<div class="form-group">
									<input class="form-control form-control-subject" name="subject" id="subject" 
									placeholder="Subject" required>
								</div>
							</div><!-- Col end -->

							<div class="col-xs-12 col-md-6">
								<div class="form-group">
									<select id="expertise" class="selectpicker form-control show-menu-arrow">
										<option>Air Transportation</option>
										<option>Ocean Transportation</option>
										<option>Road Transportation</option>
										<option>Ground Transport Housing</option>
										<option>War Housing</option>
									</select>

								</div>
							</div><!-- Col end -->
						</div><!-- row end -->
							
						<div class="form-group">
							<textarea class="form-control form-control-message" name="message" id="message" placeholder="Message" rows="8" required></textarea>
						</div>
						<div class="text-right"><br>
							<button class="btn btn-primary solid blank" type="submit">Send Message</button> 
						</div>
					</form>
					
				</div><!-- Block right -->

			</div><!--/ Content row end -->
		</div><!--/ Container end -->
	</section><!-- Content end -->

	<!-- Partners start -->
	<section id="partners-area" class="partners-area">
		<div class="container">
			<div class="row text-center">
				<h2 class="border-title">Our Clients</h2>
				<p class="border-sub-title">
					Some of our happy clients who have enjoyed our services
				</p>
			</div><!--/ Title row end -->

			<div class="row">
				<div id="partners-carousel" class="col-sm-12 owl-carousel owl-theme text-center partners">
				  <figure class="item partner-logo">
					 <a href="#">
						<img class="img-responsive" src="images/our-clients/1.jpg" alt="client">
					 </a>
				  </figure>				  
				  <figure class="item partner-logo">
					 <a href="#">
						<img class="img-responsive" src="images/our-clients/2.png" alt="client">
					 </a>
				  </figure>				  
				  <figure class="item partner-logo">
					 <a href="#">
						<img class="img-responsive" src="images/our-clients/3.png" alt="client">
					 </a>
				  </figure>				  
				  <figure class="item partner-logo">
					 <a href="#">
						<img class="img-responsive" src="images/our-clients/4.jpg" alt="client">
					 </a>
				  </figure>				  
				  <figure class="item partner-logo">
					 <a href="#">
						<img class="img-responsive" src="images/our-clients/5.png" alt="client">
					 </a>
				  </figure>				  
				  <figure class="item partner-logo">
					 <a href="#">
						<img class="img-responsive" src="images/our-clients/6.jpg" alt="client">
					 </a>
				  </figure>				  
				  <figure class="item partner-logo">
					 <a href="#">
						<img class="img-responsive" src="images/our-clients/7.png" alt="client">
					 </a>
				  </figure>				  
				  <figure class="item partner-logo">
					 <a href="#">
						<img class="img-responsive" src="images/our-clients/8.png" alt="client">
					 </a>
				  </figure>				  
				  <figure class="item partner-logo">
					 <a href="#">
						<img class="img-responsive" src="images/our-clients/9.png" alt="client">
					 </a>
				  </figure>				  
				  <figure class="item partner-logo">
					 <a href="#">
						<img class="img-responsive" src="images/our-clients/10.gif" alt="client">
					 </a>
				  </figure>				  
				  <figure class="item partner-logo">
					 <a href="#">
						<img class="img-responsive" src="images/our-clients/11.png" alt="client">
					 </a>
				  </figure>				  
				  <figure class="item partner-logo">
					 <a href="#">
						<img class="img-responsive" src="images/our-clients/12.png" alt="client">
					 </a>
				  </figure>				  
				  <figure class="item partner-logo">
					 <a href="#">
						<img class="img-responsive" src="images/our-clients/13.png" alt="client">
					 </a>
				  </figure>				  
				</div><!--/ Owl carousel end -->

			</div><!--/ Content row end -->
		</div><!--/ Container end -->
	</section><!--/ Partners end -->
@endsection 