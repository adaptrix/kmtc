@extends('pages.services.services')

@section('main-container')

<div class="col-lg-9 col-md-9 col-sm-12">
    <div class="content-inner-page"> 
        <h2 class="border-title border-left">{{isset($service_name)?$service_name:"Services"}}</h2> 
        <div class="row">
            <div class="col-md-12">
                <p>Having a resource available to fix aging equipment is imperative for today’s high-tech, fast moving companies. The problem is that much of today’s equipment won’t be supported when you need it in the future as problems start to arise. KIM TECH has a long track record of success working with companies in this position, and stepping in to troubleshoot their equipment for issues when customers are unable to contact the manufacturer or find a viable replacement. Whether its power quality related or something else, KIM TECH has the knowledge and expertise to properly identify the problem, outline the steps needed for correction, and make the necessary adjustments to fix the issue.</p>
            </div><!-- col end -->
        </div><!-- 1st row end-->

        <div class="gap-40"></div>

        <div class="row">
            <div class="col-md-6">
                <h3 class="border-title border-left">MECHANICAL INSPECTION</h3> 
                <ul class="list-arrow">
                    <li>Moisture</li>
                    <li>Wiring</li>
                    <li>Moving Parts</li>
                    <li>Insulator Checks</li> 
                </ul>
            </div>
            <div class="col-md-6">
                <h3 class="border-title border-left">KIM TECH HAS REPAIRED</h3> 
                <ul class="list-arrow">
                    <li>Building Management Systems</li>  
                    <li>Control & Monitoring Systems</li>  
                    <li>Automation Systems</li>  
                    <li>HVAC & ECU Systems</li>  
                    <li>Power Quality Systems</li>  
                    <li>Power Quality Systems</li>  
                    <li>Mechanical Systems</li>  
                    <li>Electrical Systems</li>  
                </ul>
            </div>
        </div>
    </div>
    <!--2nd row end -->

    <div class="gap-40"></div>

    <div class="call-to-action classic">
        <div class="row">
            <div class="col-md-9">
                <div class="call-to-action-text">
                    <h3 class="action-title">Interested with this service. Want to work with us for your
                        Next Project?</h3>
                </div>
            </div><!-- Col end -->
            <div class="col-md-3">
                <div class="call-to-action-btn">
                    <a class="btn btn-dark" href="{{url('contact')}}">Get a Quote</a>
                </div>
            </div><!-- col end -->
        </div><!-- row end -->
    </div><!-- Action end -->

</div><!-- Content inner end -->

@endsection