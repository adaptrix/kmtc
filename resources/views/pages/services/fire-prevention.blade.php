@extends('pages.services.services')

@section('main-container')

<div class="col-lg-9 col-md-9 col-sm-12">
    <div class="content-inner-page"> 
        <h2 class="border-title border-left">{{isset($service_name)?$service_name:"Services"}}</h2> 
        <div class="row">
            <div class="col-md-12">
                <p>Diverse design capabilities, while upholding the highest level of fire protection engineering principles, is what separates KIM TECH from our competition.</p>
                <p>We are dedicated to providing our clients with professional drawings accompanied by complete system layouts.  All electrical drawings and connection diagrams are developed in alignment with the projects requirements.</p>
            </div><!-- col end -->
        </div><!-- 1st row end-->

        <div class="gap-40"></div>

        <div class="row">
            <div class="col-md-6">
                <h3 class="border-title border-left">PRODUCT AND SERVICES</h3> 
                <ul class="list-arrow">
                    <li>Carbon Dioxide (CO2) Suppression Systems.e</li>
                    <li>FM-200 Suppression Systems.</li>
                    <li>Clean Agent Suppression Systems.</li> 
                </ul>
            </div>
            <div class="col-md-6">
                <h3 class="border-title border-left">OTHER SERVICES OFFERED INCLUDE</h3> 
                <ul class="list-arrow"> 
                    <li>Application engineering</li>  
                    <li>Site surveys</li>  
                    <li>Hazard analysis</li>  
                    <li>On-site project technical support</li>  
                    <li>Fire Appliances and Fire Extinguisher Services.</li>  
                    <li>Fire Exit Doors</li>  
                </ul>
            </div>
        </div>
    </div>
    <!--2nd row end -->

    <div class="gap-40"></div>

    <div class="call-to-action classic">
        <div class="row">
            <div class="col-md-9">
                <div class="call-to-action-text">
                    <h3 class="action-title">Interested with this service. Want to work with us for your
                        Next Project?</h3>
                </div>
            </div><!-- Col end -->
            <div class="col-md-3">
                <div class="call-to-action-btn">
                    <a class="btn btn-dark" href="{{url('contact')}}">Get a Quote</a>
                </div>
            </div><!-- col end -->
        </div><!-- row end -->
    </div><!-- Action end -->

</div><!-- Content inner end -->

@endsection