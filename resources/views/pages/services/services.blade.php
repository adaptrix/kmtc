 @extends('layouts.app')
 @section('contents')
 {{-- <div id="banner-area" class="banner-area" style="background-color:darkslateblue">
 {{-- <div id="banner-area" class="banner-area" style="background-image:url({{asset('images/banner/banner4.jp')}})"> --}
     <div class="banner-text">
         <div class="container">
             <div class="row">
                 <div class="col-xs-12">
                     <div class="banner-heading">
                         <h1 class="border-title border-left">{{isset($service_name)?$service_name:"Services"}}</h1>
                     </div>
                 </div><!-- Col end -->
             </div><!-- Row end -->
         </div><!-- Container end -->
     </div><!-- Banner text end -->
 </div><!-- Banner area end --> --}}
 <section id="main-container" class="main-container">
     <div class="container">
         <div class="row">
             @include('pages.services.menu')
             @yield('main-container')
         </div><!-- Content Col end -->
     </div><!-- Main row end -->
     </div><!-- Conatiner end -->
 </section><!-- Main container end -->
 @endsection