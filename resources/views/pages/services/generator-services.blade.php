@extends('pages.services.services')

@section('main-container')

<div class="col-lg-9 col-md-9 col-sm-12">
    <div class="content-inner-page"> 
        <h2 class="border-title border-left">{{isset($service_name)?$service_name:"Services"}}</h2> 
        <div class="row">
            <div class="col-md-12">
                <p>We offer service and maintenance plans for commercial-grade industrial generators and equipment. 
                        Using our maintenance plans allows you to extend the life of your generator and have reliable power when you need it the most.
                        Choose the plan that is right for you from our services below or give us a call for a custom option.
                        </p>
            </div><!-- col end -->
        </div><!-- 1st row end-->

        <div class="gap-40"></div>

        <div class="row">
            <div class="col-md-12">
                <h3 class="border-title border-left">Minor service maintenance</h3> 
                <p>Performed monthly, quarterly, semi-annually or tailored for your application and includes inspection of the following</p>
                <div class="row">
                    <div class="col-md-6">
                        <ul class="list-arrow">
                            <li>Generator end and controls</li> 
                            <li>Intake, exhaust system and ducts</li> 
                            <li>External charging system</li> 
                            <li>Fuel storage system</li> 
                            <li>Remote fuel systems</li> 
                        </ul>
                    </div> 
                    <div class="col-md-6">
                        <ul class="list-arrow">
                            <li>Engine and controls</li>  
                            <li>Transfer switches</li>  
                            <li>Starting system</li>  
                            <li>Cooling system</li>  
                            <li>Generator set package</li>  
                        </ul>
                    </div> 
                </div>                
            </div>
            <div class="gap-40"></div>
            <div class="col-md-12">
                <h3 class="border-title border-left">Major service maintenance</h3> 
                <p>Major Service Maintenance includes the comprehensive Minor Service Maintenance plus oil and fluid filter change. Performed annually, or every 200 hours, whichever comes first.</p>              
            </div>
            
            <div class="gap-40"></div>
            <div class="col-md-12">
                <h3 class="border-title border-left">ADDITIONAL SERVICE PLANS:</h3> 
                <div class="row">
                    <div class="col-md-12">
                        <ul class="list-arrow">
                            <li>Load Bank Service verifies the generator set integrity is up to rated output and assists your business in meeting regulatory standards</li>  
                            <li>UPS Service ensures your system will be kept continuously online.</li>  
                            <li>Motor controls cleaning and service removes incursions and prevents degradation of your motor.</li>  
                            <li>Fuel Cleaning keeps your fuel free from accumulation of water, rust, and bacteria.</li>  
                            <li>Cooling System Service is performed every six years according to manufacturers’ specifications and includes: All belts and coolant hoses; hose clamps; radiator cap; coolant thermostat and seals; remove and replace antifreeze; and pressure test of entire system.</li>  
                            <li>Any custom service plan you need, we can deliver. Call us today to discuss how we can customize a plan according to your requirements and/or needs.</li>  
                        </ul>
                    </div>  
                </div>                
            </div> 
        </div>
    </div>
    <!--2nd row end -->

    <div class="gap-40"></div>

    <div class="call-to-action classic">
        <div class="row">
            <div class="col-md-9">
                <div class="call-to-action-text">
                    <h3 class="action-title">Interested with this service. Want to work with us for your
                        Next Project?</h3>
                </div>
            </div><!-- Col end -->
            <div class="col-md-3">
                <div class="call-to-action-btn">
                    <a class="btn btn-dark" href="{{url('contact')}}">Get a Quote</a>
                </div>
            </div><!-- col end -->
        </div><!-- row end -->
    </div><!-- Action end -->

</div><!-- Content inner end -->

@endsection