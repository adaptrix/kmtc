<div class="col-lg-3 col-md-3 col-sm-12">
    <div class="sidebar sidebar-left">
        <div class="widget">
            <ul class="nav nav-tabs nav-stacked service-menu"> 
                <li class="{{Request::is('services/contract')?'active':''}}">
                    <a href="{{url('services/contract')}}">Electrical contracting services</a>
                </li> 
                <li class="{{Request::is('services/k-track')?'active':''}}">
                    <a href="{{url('services/k-track')}}">K track</a>
                </li> 
                <li class="{{Request::is('services/solar-heating')?'active':''}}">
                    <a href="{{url('services/solar-heating')}}">Solar heating</a>
                </li> 
                <li class="{{Request::is('services/laundry-equipment')?'active':''}}">
                    <a href="{{url('services/laundry-equipment')}}">Laundry Equipment</a>
                </li> 
                <li class="{{Request::is('services/fire-prevention')?'active':''}}">
                    <a href="{{url('services/fire-prevention')}}">Fire prevention services</a>
                </li> 
                <li class="{{Request::is('services/generator-services')?'active':''}}">
                    <a href="{{url('services/generator-services')}}">Generator services</a>
                </li> 
                <li class="{{Request::is('services/installation-and-commisioning')?'active':''}}">
                    <a href="{{url('services/installation-and-commisioning')}}">Installation and commisioning</a>
                </li> 
                <li class="{{Request::is('services/systems')?'active':''}}">
                    <a href="{{url('services/systems')}}">Systems trouble shoot and repair</a>
                </li> 
                <li class="{{Request::is('services/switch-gear')?'active':''}}">
                    <a href="{{url('services/switch-gear')}}">Switch gear and switch board <br> preventive maintenance</a>
                </li>  
            </ul>
        </div><!-- Widget end --> 

    </div><!-- Sidebar end -->
</div><!-- Sidebar Col end -->