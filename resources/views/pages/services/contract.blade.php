@extends('pages.services.services')

@section('main-container')

<div class="col-lg-9 col-md-9 col-sm-12">
    <div class="content-inner-page">
        <h2 class="border-title border-left">{{isset($service_name)?$service_name:"Services"}}</h2> 
  
        <div class="row">
            <div class="col-md-6"> 
                <ul class="list-arrow">
                    <li>Maintenance Shift Coverage</li> 
                    <li>Machine Commissioning</li> 
                    <li>Solar Installations</li> 
                    <li>Consulting / Training</li> 
                    <li>Industrial Electronic Repair</li> 
                    <li>Equipment Relocation/Plant Moves</li> 
                    <li>Building Management Systems</li> 
                    <li>Power Factor Correction Solutions</li> 
                </ul>
            </div>
            <div class="col-md-6"> 
                <ul class="list-arrow">
                    <li>24-hour Emergency Service</li> 
                    <li>Street lighting design, installation and maintenance.</li> 
                    <li>Electrical design-build</li> 
                    <li>Lights retrofitting</li> 
                    <li>Automatic change over switch</li> 
                    <li>Transformer preventive maintenance</li> 
                    <li>Power Factor Correction Solutions</li>  
                </ul>
            </div>
        </div>
    </div>
    <!--2nd row end -->

    <div class="gap-40"></div>
    <div class="gap-40"></div>

    <div class="call-to-action classic">
        <div class="row">
            <div class="col-md-9">
                <div class="call-to-action-text">
                    <h3 class="action-title">Interested with this service. Want to work with us for your
                        Next Project?</h3>
                </div>
            </div><!-- Col end -->
            <div class="col-md-3">
                <div class="call-to-action-btn">
                    <a class="btn btn-dark" href="{{url('contact')}}">Get a Quote</a>
                </div>
            </div><!-- col end -->
        </div><!-- row end -->
    </div><!-- Action end -->

</div><!-- Content inner end -->

@endsection