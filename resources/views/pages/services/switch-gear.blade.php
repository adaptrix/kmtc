@extends('pages.services.services')

@section('main-container')

<div class="col-lg-9 col-md-9 col-sm-12">
    <div class="content-inner-page"> 
        <h2 class="border-title border-left">{{isset($service_name)?$service_name:"Services"}}</h2> 
        <div class="row">
            <div class="col-md-12">
                <p>Power Systems & Controls, Switchgear, Switchboard, and Circuit Breaker Preventive Maintenance will help prevent facility downtime and Overall Equipment Effectiveness (OEE). In the more extreme conditions, a facility could experience not only downtime, but potentially a fire, explosion and or a catastrophic failure to additional equipment both upstream and downstream of the circuit protective devices being maintained.</p>
            </div><!-- col end -->
        </div><!-- 1st row end-->

        <div class="gap-40"></div>

        <div class="row">
            <div class="col-md-6">
                <h3 class="border-title border-left">MECHANICAL INSPECTION</h3> 
                <ul class="list-arrow">
                    <li>Moisture</li>
                    <li>Wiring</li>
                    <li>Moving Parts</li>
                    <li>Insulator Checks</li> 
                </ul>
            </div>
            <div class="col-md-6">
                <h3 class="border-title border-left">ELECTRICAL TEST</h3> 
                <ul class="list-arrow">
                    <li>Power Quality</li> 
                    <li>Primary and Secondary Injection</li> 
                    <li>Wiring</li> 
                    <li>Arc Flash Analysis</li> 
                    <li>Infrared Scan</li> 
                    <li>Circuit Breakers and Switches</li> 
                </ul>
            </div>
        </div>
    </div>
    <!--2nd row end -->

    <div class="gap-40"></div>

    <div class="call-to-action classic">
        <div class="row">
            <div class="col-md-9">
                <div class="call-to-action-text">
                    <h3 class="action-title">Interested with this service. Want to work with us for your
                        Next Project?</h3>
                </div>
            </div><!-- Col end -->
            <div class="col-md-3">
                <div class="call-to-action-btn">
                    <a class="btn btn-dark" href="{{url('contact')}}">Get a Quote</a>
                </div>
            </div><!-- col end -->
        </div><!-- row end -->
    </div><!-- Action end -->

</div><!-- Content inner end -->

@endsection