@extends('pages.services.services')

@section('main-container')

<div class="col-lg-9 col-md-9 col-sm-12">
    <div class="content-inner-page"> 
        <h2 class="border-title border-left">{{isset($service_name)?$service_name:"Services"}}</h2> 
        <div class="row">
            <div class="col-md-12">  
                <p>Solar water heating (SWH) is the conversion of sunlight into heat for water heating using a solar thermal collector. A variety of configurations are available at varying cost to provide solutions in different climates and latitudes.</p>
                <div class="row">
                    <div class="col-md-12">
                        <ul class="list-arrow">
                            <li>preheated solar heating systems</li>   
                            <li>pressurized solar water heater</li>   
                            <li>Unpressurized s.w.h</li>   
                            <li>Separate pressure bearing system</li>   
                        </ul>
                    </div>  
                </div>                
            </div>

            <div class="gap-40"></div> 
        </div>
    </div>
    <!--2nd row end -->

    <div class="gap-40"></div>

    <div class="call-to-action classic">
        <div class="row">
            <div class="col-md-9">
                <div class="call-to-action-text">
                    <h3 class="action-title">Interested with this service. Want to work with us for your
                        Next Project?</h3>
                </div>
            </div><!-- Col end -->
            <div class="col-md-3">
                <div class="call-to-action-btn">
                    <a class="btn btn-dark" href="{{url('contact')}}">Get a Quote</a>
                </div>
            </div><!-- col end -->
        </div><!-- row end -->
    </div><!-- Action end -->

</div><!-- Content inner end -->

@endsection