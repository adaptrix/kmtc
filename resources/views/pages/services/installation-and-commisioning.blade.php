@extends('pages.services.services')

@section('main-container')

<div class="col-lg-9 col-md-9 col-sm-12">
    <div class="content-inner-page"> 
        <h2 class="border-title border-left">{{isset($service_name)?$service_name:"Services"}}</h2> 
        <div class="row">
            <div class="col-md-12">
                <p>We are providing Air Conditioner installation and refrigeration systems. We provide best services like split air conditioner installation, ducted split air conditioner installation, package air conditioner installation and central air conditioner installation. We have experience of more than four houses villas and more than two buildings. So go with the experts like us and get the best installation services.</p>
            </div><!-- col end -->
        </div><!-- 1st row end-->
 

    <div class="gap-40"></div>

    <div class="call-to-action classic">
        <div class="row">
            <div class="col-md-9">
                <div class="call-to-action-text">
                    <h3 class="action-title">Interested with this service. Want to work with us for your
                        Next Project?</h3>
                </div>
            </div><!-- Col end -->
            <div class="col-md-3">
                <div class="call-to-action-btn">
                    <a class="btn btn-dark" href="{{url('contact')}}">Get a Quote</a>
                </div>
            </div><!-- col end -->
        </div><!-- row end -->
    </div><!-- Action end -->

</div><!-- Content inner end -->

@endsection