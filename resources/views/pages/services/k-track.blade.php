@extends('pages.services.services')

@section('main-container')

<div class="col-lg-9 col-md-9 col-sm-12">
    <div class="content-inner-page"> 
        <h2 class="border-title border-left">{{isset($service_name)?$service_name:"Services"}}</h2> 
        <div class="row">
            <div class="col-md-12">
                <h3>Minor service maintenance</h3> 
                <p>K Track comes with many features that can help companies from different industries. We are experts in GPS tracking field with years of experience and deliver advanced telematics solutions. Our products can help you to reduce operation costs of your fleet and business; and improve its efficiency. </p>
                <p>We provide tailored tracking solutions for big and small, private and commercial clients.</p>
                <div class="row">
                    <div class="col-md-6">
                        <ul class="list-arrow">
                            <li>Task Management</li>  
                            <li>Reports</li>  
                            <li>Geo fences</li>  
                            <li>Vehicle engine blocking</li>  
                            <li>Safety button</li>  
                            <li>Behavior analysis</li>  
                            <li>Alerts</li>  
                        </ul>
                    </div> 
                    <div class="col-md-6">
                        <ul class="list-arrow">
                            <li>Task management</li>
                            <li>Tachograph prediction</li>
                            <li>Refrigerator temperature</li>
                            <li>Remote tachograph download</li>
                            <li>Fuel control</li>
                            <li>GPS tracking</li>
                        </ul>
                    </div> 
                </div>                
            </div>

            <div class="gap-40"></div> 
        </div>
    </div>
    <!--2nd row end -->

    <div class="gap-40"></div>

    <div class="call-to-action classic">
        <div class="row">
            <div class="col-md-9">
                <div class="call-to-action-text">
                    <h3 class="action-title">Interested with this service. Want to work with us for your
                        Next Project?</h3>
                </div>
            </div><!-- Col end -->
            <div class="col-md-3">
                <div class="call-to-action-btn">
                    <a class="btn btn-dark" href="{{url('contact')}}">Get a Quote</a>
                </div>
            </div><!-- col end -->
        </div><!-- row end -->
    </div><!-- Action end -->

</div><!-- Content inner end -->

@endsection