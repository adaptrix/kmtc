@extends('layouts.app')

@section('contents')

<div id="banner-area" class="banner-area" style="background-image:url(images/banner/banner1.jpg)">
    <div class="banner-text">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="banner-heading">
                        <h1 class="border-title border-left">About Us</h1>
                        <ol class="breadcrumb">
                            <li><a href="{{url('/')}}">Home</a></li>
                            <li>About Us</li>
                        </ol>
                    </div>
                </div><!-- Col end -->
            </div><!-- Row end -->
        </div><!-- Container end -->
    </div><!-- Banner text end -->
</div><!-- Banner area end -->


<section id="main-container" class="main-container">
    <div class="container">

        <div class="row">
            <div class="col-md-6">
                <h3>Company Overview</h3>
				<p>Established in 2015 in Dar-es-Salaam, Tanzania and later expanded a branch to Arusha. Kim Tech Company Limited has over years of experience in delivering high quality services to enable clients to meet the growth challenges of quality and assurance. </p>
				
				<p>We as a company can reduce risk and improve compliance. Our accreditations offer customers peace of mind.</p>
				<p>Our continued business is built on reputation and reliability, whilst meeting and exceeding our client’s expectations.</p>
				<p>With our employees having knowledge of regulations, great technical ability and experience of working in a wide range of industries, we can minimize your costs and fulfil your requirements.</p>
				<p>Our success has enabled us to reach more customers and complete more projects since the inception of the company.</p>
            </div><!-- Col end -->

            <div class="col-md-6">
                <h3>Our Core Values</h3>
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Our mission</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <p>"Electrical energy is a cornerstone of our quality of life. There will be a continuing need for products and services to harness its use. Kim Tech Company will strive to be best-in-class as a distributor of quality products and services.”</p>
                            </div>
                        </div>
                    </div>
                    <!--/ Panel 1 end-->

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion"
                                    href="#collapseTwo"> Our vision</a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>We are committed to providing the highest quality products, competitively priced, with services exceeding our customer’s expectations. We will continue to invest in facilities, systems and highly trained technical personnel providing added-value to our business relationships.</p>
                            </div>
                        </div>
                    </div>
                    <!--/ Panel 2 end-->
 

                </div>
                <!--/ Accordion end -->

            </div><!-- Col end -->
        </div><!-- Content row end -->
    </div><!-- Conatiner end -->
</section><!-- Main container end -->


<section class="pattern-bg">
    <div class="container">
        <div class="row text-center">
            <h2 class="border-title">Our Best Practices</h2>
            <p class="border-sub-title">
                Collaboratively administrate empowered markets via plug-and-play networks.
            </p>
        </div>
        <!--/ Title row end -->

        <div class="row">
            <div class="col-md-3">
                <div class="ts-service-box text-center">
                    <div class="ts-service-icon">
                        <i class="fa fa-users"></i>
                    </div>
                    <div class="ts-service-box-info">
                        <h3 class="service-box-title"><a href="#">Professional Team</a></h3>
                        <p>With the best trained professionals in technical and engineering practices we easily deliver high standard</p>
                    </div>
                </div>
            </div><!-- Col 1 end -->

            <div class="col-md-3">
                <div class="ts-service-box text-center">
                    <div class="ts-service-icon">
                        <i class="fa fa-thumbs-up"></i>
                    </div>
                    <div class="ts-service-box-info">
                        <h3 class="service-box-title"><a href="#">Responsibility</a></h3>
                        <p>We are commited to each every project, to meet quality and standard irrespective of the scope</p>
                    </div>
                </div>
            </div><!-- Col 2 end -->
            <div class="col-md-3">
                <div class="ts-service-box text-center">
                    <div class="ts-service-icon">
                        <i class="fa fa-tachometer"></i>
                    </div>
                    <div class="ts-service-box-info">
                        <h3 class="service-box-title"><a href="#">Focus On Time</a></h3>
                        <p>We understand the essence of timeliness and effective management to ensure that we are always ahead of deadlines</p>
                    </div>
                </div>
            </div><!-- Col 3 end -->

            <div class="col-md-3">
                <div class="ts-service-box text-center">
                    <div class="ts-service-icon">
                        <i class="fa fa-diamond"></i>
                    </div>
                    <div class="ts-service-box-info">
                        <h3 class="service-box-title"><a href="#">Diversity</a></h3>
                        <p>We are very flexible towards our clients needs, with the professionals we have aboard, we can extend our services</p>
                    </div>
                </div>
            </div><!-- Col 4 end -->

        </div>
        <!--/ Content row end -->
    </div>
    <!--/ Container end -->
</section><!-- Facts end -->


{{-- <section id="ts-team" class="ts-team">
    <div class="container">
        <div class="row text-center">
            <h2 class="border-title">Our Leadership</h2>
            <p class="border-sub-title">
                Collaboratively administrate empowered markets via plug-and-play networks.
            </p>
        </div>
        <!--/ Title row end -->

        <div class="row">
            <div id="team-slide" class="owl-carousel owl-theme team-slide">
                <div class="item">
                    <div class="ts-team-wrapper">
                        <div class="team-img-wrapper">
                            <img alt="" src="images/team/team1.jpg" class="img-responsive">
                        </div>
                        <div class="ts-team-content">
                            <h3 class="ts-name">Nats Stenman</h3>
                            <p class="ts-designation">Business Director</p>
                            <p class="ts-description">Nats Stenman began his career in construction with boots on the
                                ground</p>
                            <div class="team-social-icons">
                                <a target="_blank" href="#"><i class="fa fa-facebook"></i></a>
                                <a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                                <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a>
                                <a target="_blank" href="#"><i class="fa fa-linkedin"></i></a>
                            </div>
                            <!--/ social-icons-->
                        </div>
                    </div>
                    <!--/ Team wrapper end -->
                </div><!-- Team 1 end -->

                <div class="item">
                    <div class="ts-team-wrapper">
                        <div class="team-img-wrapper">
                            <img alt="" src="images/team/team2.jpg" class="img-responsive">
                        </div>
                        <div class="ts-team-content">
                            <h3 class="ts-name">Mark Conter</h3>
                            <p class="ts-designation">Sales Director</p>
                            <p class="ts-description">Nats Stenman began his career in construction with boots on the
                                ground</p>
                            <div class="team-social-icons">
                                <a target="_blank" href="#"><i class="fa fa-facebook"></i></a>
                                <a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                                <a target="_blank" href="#"><i class="fa fa-linkedin"></i></a>
                            </div>
                            <!--/ social-icons-->
                        </div>
                    </div>
                    <!--/ Team wrapper end -->
                </div><!-- Team 2 end -->

                <div class="item">
                    <div class="ts-team-wrapper">
                        <div class="team-img-wrapper">
                            <img alt="" src="images/team/team3.jpg" class="img-responsive">
                        </div>
                        <div class="ts-team-content">
                            <h3 class="ts-name">Angela Lyouer</h3>
                            <p class="ts-designation">Business Dev. Manager</p>
                            <p class="ts-description">Nats Stenman began his career in construction with boots on the
                                ground</p>
                            <div class="team-social-icons">
                                <a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                                <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a>
                                <a target="_blank" href="#"><i class="fa fa-linkedin"></i></a>
                            </div>
                            <!--/ social-icons-->
                        </div>
                    </div>
                    <!--/ Team wrapper end -->
                </div><!-- Team 3 end -->

                <div class="item">
                    <div class="ts-team-wrapper">
                        <div class="team-img-wrapper">
                            <img alt="" src="images/team/team4.jpg" class="img-responsive">
                        </div>
                        <div class="ts-team-content">
                            <h3 class="ts-name">Dave Clarkte</h3>
                            <p class="ts-designation">Regional Manager</p>
                            <p class="ts-description">Nats Stenman began his career in construction with boots on the
                                ground</p>
                            <div class="team-social-icons">
                                <a target="_blank" href="#"><i class="fa fa-facebook"></i></a>
                                <a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                                <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a>
                                <a target="_blank" href="#"><i class="fa fa-linkedin"></i></a>
                            </div>
                            <!--/ social-icons-->
                        </div>
                    </div>
                    <!--/ Team wrapper end -->
                </div><!-- Team 4 end -->

                <div class="item">
                    <div class="ts-team-wrapper">
                        <div class="team-img-wrapper">
                            <img alt="" src="images/team/team5.jpg" class="img-responsive">
                        </div>
                        <div class="ts-team-content">
                            <h3 class="ts-name">Ayesha Stewart</h3>
                            <p class="ts-designation">Senior Recruiter</p>
                            <p class="ts-description">Nats Stenman began his career in construction with boots on the
                                ground</p>
                            <div class="team-social-icons">
                                <a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                                <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a>
                                <a target="_blank" href="#"><i class="fa fa-linkedin"></i></a>
                            </div>
                            <!--/ social-icons-->
                        </div>
                    </div>
                    <!--/ Team wrapper end -->
                </div><!-- Team 5 end -->

                <div class="item">
                    <div class="ts-team-wrapper">
                        <div class="team-img-wrapper">
                            <img alt="" src="images/team/team6.jpg" class="img-responsive">
                        </div>
                        <div class="ts-team-content">
                            <h3 class="ts-name">Elsie Gaeta</h3>
                            <p class="ts-designation">Marketing Specialist</p>
                            <p class="ts-description">Nats Stenman began his career in construction with boots on the
                                ground</p>
                            <div class="team-social-icons">
                                <a target="_blank" href="#"><i class="fa fa-facebook"></i></a>
                                <a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                                <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a>
                                <a target="_blank" href="#"><i class="fa fa-linkedin"></i></a>
                            </div>
                            <!--/ social-icons-->
                        </div>
                    </div>
                    <!--/ Team wrapper end -->
                </div><!-- Team 6 end -->

            </div><!-- Team slide end -->

        </div>
        <!--/ Content row end -->
    </div>
    <!--/ Container end -->
</section> --}}
<!--/ Team end -->

@endsection