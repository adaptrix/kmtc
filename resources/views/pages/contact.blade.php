@extends('layouts.app')

@section('contents')  

<div id="banner-area" class="banner-area" style="background-image:url(images/banner/banner1.jpg)">
    <div class="banner-text">
         <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="banner-heading">
                        <h1 class="border-title border-left">Contact</h1>
                        <ol class="breadcrumb">
                            <li><a href="{{url('/')}}">Home</a></li>
                            <li>Contact</li> 
                        </ol>
                    </div>
                </div><!-- Col end -->
            </div><!-- Row end -->
       </div><!-- Container end -->
    </div><!-- Banner text end -->
</div><!-- Banner area end --> 

<section id="main-container" class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="sidebar sidebar-left">
                    <div class="widget contact-info">
                        <h3 class="widget-title">ARUSHA</h3>

                        <div class="contact-info-box">
                            <i class="fa fa-map">&nbsp;</i>
                            <div class="contact-info-box-content">
                                <h4>Visit Us</h4>
                                <p>NHC BLOCKS, <br> Mwanza Street, <br> Arusha, Tanzania  </p>
                            </div>
                        </div>

                        <div class="contact-info-box">
                            <i class="fa fa-envelope">&nbsp;</i>
                            <div class="contact-info-box-content">
                                <h4>Mail Us</h4>
                                <p>info@kimtech.co.tz</p>
                            </div>
                        </div>

                        <div class="contact-info-box">
                            <i class="fa fa-phone-square">&nbsp;</i>
                            <div class="contact-info-box-content">
                                <h4>Call Us</h4>
                                <p>+255 753 513 125 <br> +255 652 637 389</p>
                            </div>
                        </div>

                    </div><!-- Widget end -->
                </div><!-- Sidebar left end -->
            </div><!-- Sidebar col end -->
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="sidebar sidebar-left">
                    <div class="widget contact-info">
                        <h3 class="widget-title">DAR-ES-SALAAM</h3>

                        <div class="contact-info-box">
                            <i class="fa fa-map">&nbsp;</i>
                            <div class="contact-info-box-content">
                                <h4>Visit Us</h4> 
                                <p>243 Toure drive, <br> Oyster-Bay, <br>Dar-es-salaam, Tanzania. </p>
                            </div>
                        </div>

                        <div class="contact-info-box">
                            <i class="fa fa-envelope">&nbsp;</i>
                            <div class="contact-info-box-content">
                                <h4>Mail Us</h4>
                                <p>info@kimtech.co.tz</p>
                            </div>
                        </div>

                        <div class="contact-info-box">
                            <i class="fa fa-phone-square">&nbsp;</i>
                            <div class="contact-info-box-content">
                                <h4>Call Us</h4>
                                <p>+255 784 830 632 <br> +255 766 264 000</p>
                            </div>
                        </div>

                    </div><!-- Widget end -->
                </div><!-- Sidebar left end -->
            </div><!-- Sidebar col end -->

            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <h3 class="border-title border-left">Contact Form</h3>
                <form id="contact-form" action="http://themewinter.com/html/cornike/logistic/contact-form.php" method="post" role="form">
                    <div class="error-container"></div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Name</label>
                            <input class="form-control form-control-name" name="name" id="name" placeholder="" type="text" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Email</label>
                                <input class="form-control form-control-email" name="email" id="email" 
                                placeholder="" type="email" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Subject</label>
                                <input class="form-control form-control-subject" name="subject" id="subject" 
                                placeholder="" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Message</label>
                        <textarea class="form-control form-control-message" name="message" id="message" placeholder="" rows="10" required></textarea>
                    </div>
                    <div class="text-right"><br>
                        <button class="btn btn-primary solid blank" type="submit">Send Message</button> 
                    </div>
                </form>
            </div>
        
        </div><!-- Content row -->
    </div><!-- Conatiner end -->
</section><!-- Main container end -->
@endsection 